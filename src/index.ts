import * as Async from "@altitude/async";
import {
    AutoBusyState,
    IBusyStateProvider,
    LoadingState
} from "@altitude/busy-state";
import { Disposables, IDisposer } from "@altitude/disposables";
import * as ErrorMessage from "@altitude/error-message";
import { Monitor } from "@altitude/semaphore";
import { isObservable, Observable, Subject } from "rxjs";

const VM_TYPE_NAME = "ViewModel";
const CONSTRUCT_METHOD_NAME = "construct";
const INIT_METHOD_NAME = "init";
const INITIALISING_STATE = "initialising";
const ROOT_NODE_NAME = "root";

// tslint:disable: max-classes-per-file

export type ViewModelDependency = ViewModel | Observable<any>;

class Common<TData = any> {
    get disposables(): Disposables {
        if (this._disposables == undefined) {
            this._disposables = new Disposables();
        }

        return this._disposables;
    }

    get disposer(): IDisposer {
        if (this._disposer == undefined) {
            this._disposer = this.disposables.createDisposer();
        }

        return this._disposer;
    }
    // tslint:disable-next-line: no-object-literal-type-assertion
    readonly data: TData = <TData>{};
    readonly globalState = new AutoBusyState();
    intialised = false;
    readonly loaded$: Observable<void>;
    private _disposables: Disposables;
    private _disposer: IDisposer;
    private _error: Subject<any> | undefined;
    private _error$: Observable<any> | undefined;

    constructor(onLoaded: () => void) {
        const loaded = new Subject<void>();
        this.loaded$ = Async.observeOnce(loaded);

        this.disposables.addSubscription(
            this.globalState.loadingState$.subscribe(state => {
                if (state === LoadingState.Loaded) {
                    onLoaded();
                    loaded.next();
                }
            })
        );

        this.disposables.subscribeTo(this.globalState.error$, e => {
            this.raiseError(e);
        });
    }

    get error$(): Observable<any> {
        if (this._error$ == undefined) {
            this._error = new Subject<any>();
            this._error$ = this._error.asObservable();
        }

        return this._error$;
    }

    dispose(): void {
        this.globalState.dispose();

        if (this._disposables != undefined) {
            this._disposables.dispose();
        }
    }

    raiseError(e: any): void {
        const msg =
            "An unhandled error occurred during an asynchronous ViewModel background operation.";
        // tslint:disable-next-line: no-console
        console.error(msg, e);

        if (this._error != undefined) {
            this._error.next(e);
        }
    }
}

/**
 * The base class for view-model implementations.
 */
export abstract class ViewModel<TData = any> {
    /**
     * Returns a reference to the instance's `BusyStateProvider`.
     *
     * This property will throw if referenced before the `construct` method has been invoked on the agggregate root.
     */
    get busyState(): IBusyStateProvider {
        this.disposedGuard();
        this.postConstrGuard();

        return this._common.globalState;
    }
    /**
     * A global state object shared by the root view model and all its composites.
     * The data object may be used to store common state which may optionally be exposed to client code by the root
     * view model.
     *
     * This property will throw if referenced before the `construct` method has been invoked on the agggregate root.
     */
    protected get data(): TData {
        this.disposedGuard();
        this.postConstrGuard();

        return <TData>this._common.data;
    }
    /**
     * Returns a public `Disposer` wrapper for the instance's internal `Disposable` object.
     *
     * This property will throw if referenced before the `construct` method has been invoked on the agggregate root.
     */
    get disposer(): IDisposer {
        this.disposedGuard();
        this.postConstrGuard();

        return this._common.disposer;
    }
    /**
     * Emits when any monitored action in the instance's common tree errors.
     *
     * This property will throw if referenced before the `construct` method has been invoked on the agggregate root.
     */
    get error$(): Observable<any> {
        this.disposedGuard();
        this.postConstrGuard();

        return this._common.error$;
    }
    /**
     * Returns `true` if the instance's `BusyStateProvider` is in the busy state, otherwise returns `false`.
     */
    get isBusy(): boolean {
        this.disposedGuard();

        return this.isConstructed ? this.busyState.isBusy : false;
    }
    /**
     * Returns `true` if the instance is a child of another `ViewModel` in the aggregated set, otherwise
     * returns `false`.
     */
    protected get isChild(): boolean {
        this.disposedGuard();

        return this._parent != undefined;
    }

    private get isConstructed(): boolean {
        this.disposedGuard();

        return this._common != undefined;
    }
    /**
     * Returns `true` if the instance has been disposed, otherwise returns `false`.
     */
    get isDisposed(): boolean {
        return this._disposed;
    }

    private get isInitalised(): boolean {
        this.disposedGuard();

        return this.isConstructed && this._common.intialised;
    }

    private get isInitalising(): boolean {
        this.disposedGuard();

        return this.isConstructed && !this._common.intialised;
    }
    /**
     * Returns `true` if the instance's `BusyStateProvider` is in the `LoadingState.Loaded` state, otherwise
     * returns `false`.
     */
    get isLoaded(): boolean {
        this.disposedGuard();

        return this.isConstructed
            ? this.busyState.loadingState === LoadingState.Loaded
            : false;
    }

    /**
     * Returns `true` if the instance is the root `ViewModel` in the aggregated set, otherwise returns `false`.
     *
     * NOTE: This property will always return `false` until after `constructed` has been invoked.
     */
    protected get isRoot(): boolean {
        this.disposedGuard();

        return this._parent == undefined && this.isConstructed;
    }
    /**
     * Returns `true` if the instance's `BusyStateProvider` is in the `LoadingState.Loaded` state and not in
     * the busy state,
     * otherwise returns `false`.
     */
    get isStatic(): boolean {
        this.disposedGuard();

        return !this.isBusy && this.isLoaded;
    }
    /**
     * Emits once when all instance in the instance's common tree have completed their loading actions.
     *
     * This property will throw if referenced before the `construct` method has been invoked on the agggregate root.
     */
    get loaded$(): Observable<void> {
        this.disposedGuard();
        this.postConstrGuard();

        return this._common.loaded$;
    }

    private _children: ViewModel[];
    private _common: Common;
    private _disposed = false;
    private readonly _localState = new Monitor();
    private _parent: ViewModel;
    private _pendingDeps = 0;

    constructor() {
        this._localState
            .getSuccessEvent()
            .subscribe(() => this.onLocalLoaded());
    }
    /**
     * Creates internal associations between all view model instances in the aggregate tree.
     *
     * This method will throw if invoked more than once, after the `init` method has been invoked,
     * or on any view model other than the agggregate root.
     *
     * If it has not alread been invoked explicitly, `construct` will be invoked implicitly when the `init` method is
     * invoked on the aggregate root.
     */
    construct(): void {
        this.disposedGuard();
        this.preConstrGuard();
        this.isRootGuard();
        const common = new Common<TData>(() => this.onLoadedInternal());
        this.traverse(vm => {
            vm._common = common;
        });
        this._common.disposables.subscribeTo(this._localState.error$, e =>
            this._common.raiseError(e)
        );
    }
    /**
     * Releases the resources associated with the instance and its aggregates, including subscriptions to any
     * aynsc operations.
     */
    dispose(): void {
        this.traverse(vm => {
            vm._disposed = true;
            vm._localState.dispose();
        });

        if (this._common != undefined) {
            this._common.dispose();
        }
    }
    /**
     * The view model's initialisation function, which should only be called on the root `ViewModel` instance.
     *
     * This method will throw if invoked before the `construct` method has been invoked on the agggregate root.
     *
     * NOTE: The `init` method will invoke the `construct` method implictly if it has not already been called.
     */
    init(): void {
        this.disposedGuard();
        this.preInitGuard();
        this.isRootGuard();
        if (!this.isConstructed) {
            this.construct();
        }

        this._common.globalState.enterPrepare();
        this.preInit();
        this.initInternal();
        this._common.intialised = true;
        this.postInit();
        // It's possible that dispose may be called during the init sequence of a client wants to
        // cancel all other events following an error.
        if (!this._common.globalState.isDisposed) {
            this._common.globalState.exitPrepare();
        }
    }
    /**
     * Aggregates another `ViewModel` within the instance.
     *
     * This method will throw if invoked after the `construct` method has been invoked on the agggregate root.
     * @param viewModel The instance to aggregate.
     */
    protected aggregate(viewModel: ViewModel): void {
        this.disposedGuard();
        if (viewModel == undefined) {
            throw new Error(ErrorMessage.argument("viewModel"));
        }

        this.preConstrGuard();

        if (viewModel === this) {
            throw new Error(
                ErrorMessage.argument("viewModel", "Cannot aggregate itself.")
            );
        }

        if (viewModel._parent != undefined) {
            throw new Error(
                ErrorMessage.argument(
                    "viewModel",
                    "The instance has already been aggregated."
                )
            );
        }
        viewModel._parent = this;
        if (this._children == undefined) this._children = [];
        this._children.push(viewModel);
    }
    /**
     * Invoked once when the instance's `BusyStateProvider` enters the `LoadingState.Loaded` state.
     * Mey be overridden by subclasses to peform post-load initialisation.
     */
    // tslint:disable-next-line: no-empty
    protected onLoaded(): void {}
    /**
     * Invoked once when all of the instance's registered loading dependencies have completed.
     */
    // tslint:disable-next-line: no-empty
    protected onLocalLoaded(): void {}
    /**
     * Registers an `Observable` instance with the instance's `BusyStateProvider`, which will report a busy state
     * until all such registered actions have completed or errored.
     *
     * This method will throw if invoked before the `construct` method has been invoked on the agggregate root.
     * @param action The action to register.
     */
    protected registerBusy(action: Observable<any>): void {
        this.disposedGuard();
        this.postConstrGuard();
        this._common.globalState.registerBusy(action);
    }
    /**
     * Registers one or more loading actions for processing.
     *
     * The instance's `BusyStateProvider` will remain in a loading state until all registered loading actions
     * have emitted at least once or completed, or one or more has errored.
     *
     * This method will throw if it is invoked outside of the root view model's initialisation sequence.
     *
     * @param actions The actions to register.
     * @returns an `Observable` which will emit complete only when all `actions` have emitted.
     */
    protected registerLoading(...actions: Observable<any>[]): void {
        this.disposedGuard();
        this.doingInitGuard();
        this._localState.monitor(...actions);
    }
    /**
     * Registers one or more loading actions for processing after a specified set of dependencies have emitted
     * at least once or completed.
     *
     * The instance's `BusyStateProvider` will remain in a loading state until all registered loading actions
     * have emitted at least once or completed, or one or more has errored.
     *
     * This method will throw if it is invoked outside of the root view model's initialisation sequence.
     *
     * @param actions The actions to register.
     * @returns an `Observable` which will emit complete only when all `actions` have emitted.
     */
    protected registerLoadingAfter(
        dependencies: ViewModelDependency[],
        ...actions: Async.AwaitAction[]
    ): void {
        this.disposedGuard();
        this.doingInitGuard();
        if (actions.length === 0) return;
        const mapped = this.mapDependencies(dependencies);
        // Increment _pendingDeps to ensure that we don't invoke _localState.exitPrepare() prior to the action
        // being invoked and monitored.
        this._pendingDeps += 1;
        Async.subscribeOnce(Async.awaitOne(mapped), () => {
            if (this.isDisposed) return;
            actions.forEach(action => {
                const obs = action();
                if (Array.isArray(obs)) {
                    this._localState.monitor(...obs);
                } else {
                    this._localState.monitor(obs);
                }
            });
            // IMPORTANT: Decrement _pendingDeps AFTER monitoring the action result, otherwise _localState may emit prematurely.
            this._pendingDeps -= 1;
            // Try and exit the local state monitor's preparation state. Necessary in case the initialisation sequence
            // (which also tries to exit local prepare) completed before all monitored local state dependencies emitted.
            this.tryExitLocalPrepare();
        });
    }
    /**
     * Registers a sequence of loading actions.
     *
     * The instance's `BusyStateProvider` will remain in a loading state until all registered loading actions
     * have emitted at least once or completed, or one or more has errored.
     *
     * Each action in the sequence must complete before the subsequent action will commence.
     * An action may represent a collection of child actions which are not themselves guaranteed to execute in sequence.
     *
     * This method will throw if it is invoked outside of the root view model's initialisation sequence.
     *
     * @param actions The actions to register.
     * @returns an `Observable` which will emit complete only when all `actions` have emitted.
     */
    protected registerLoadingSequence(...actions: Async.AwaitAction[]): void {
        this.disposedGuard();
        this.doingInitGuard();
        if (actions.length === 0) return;
        const obs = Async.awaitSequence(...actions);
        this.registerLoading(obs);
    }
    /**
     * Registers a sequence of loading actions for processing after a specified set of dependencies have emitted
     * at least once or completed.
     *
     * The instance's `BusyStateProvider` will remain in a loading state until all registered loading actions
     * have emitted at least once or completed, or one or more has errored.
     *
     * Each action in the sequence must complete before the subsequent action will commence.
     * An action may represent a collection of child actions which are not themselves guaranteed to execute in sequence.
     *
     * This method will throw if it is invoked outside of the root view model's initialisation sequence.
     *
     * @param actions The actions to register.
     * @returns an `Observable` which will emit complete only when all `actions` have emitted.
     */
    protected registerLoadingSequenceAfter(
        dependencies: ViewModelDependency[],
        ...actions: Async.AwaitAction[]
    ): void {
        this.disposedGuard();
        this.doingInitGuard();
        if (actions.length === 0) return;
        const mapped = this.mapDependencies(dependencies);
        const action = () => Async.awaitSequence(...actions);
        this.registerLoadingAfter(mapped, action);
    }
    /**
     * Should be implemented by subclasses to perform initialisaton processing.
     * Loading dependencies may be registered from within `viewModelInit`.
     */
    protected abstract viewModelInit(): void;

    private disposedGuard(): void {
        if (this.isDisposed) {
            throw new Error(ErrorMessage.objectDisposed());
        }
    }

    private doingInitGuard(): void {
        if (!this.isInitalising) {
            throw new Error(
                ErrorMessage.invalidOpOutsideState(
                    INITIALISING_STATE,
                    VM_TYPE_NAME
                )
            );
        }
    }

    private initInternal(): void {
        this.traverse(vm => vm.viewModelInit());
    }

    private isRootGuard(): void {
        if (this.isChild) {
            throw new Error(
                ErrorMessage.invalidOpRequiresTarget(
                    ROOT_NODE_NAME,
                    VM_TYPE_NAME
                )
            );
        }
    }

    // tslint:disable-next-line: prefer-function-over-method
    private mapDependencies(
        dependencies: ViewModelDependency[]
    ): Observable<any>[] {
        if (dependencies == undefined || dependencies.length === 0) {
            return [Async.immediate()];
        }
        const r: Observable<any>[] = [];
        dependencies.forEach(d => {
            if (d instanceof ViewModel) {
                r.push(d._localState.getSuccessEvent());
            } else if (isObservable(d)) {
                r.push(d);
            }
        });

        return r;
    }

    private onLoadedInternal(): void {
        this.traverse(vm => vm.onLoaded());
    }

    private postConstrGuard(): void {
        if (!this.isConstructed) {
            throw new Error(
                ErrorMessage.invalidOpBeforeMethod(
                    CONSTRUCT_METHOD_NAME,
                    VM_TYPE_NAME
                )
            );
        }
    }

    private postInit() {
        this.traverse(vm => vm.tryExitLocalPrepare());
    }

    private preConstrGuard(): void {
        if (this.isConstructed) {
            throw new Error(
                ErrorMessage.invalidOpAfterMethod(
                    CONSTRUCT_METHOD_NAME,
                    VM_TYPE_NAME
                )
            );
        }
    }

    private preInit() {
        // We do the preInit step so that all view models in the tree remain in the local preparing state until
        // the init cycle has completed. This makes it is possible to register loading dependencies on view models
        // outside of their own viewModelInit handlers. For example, if a view model exposed a public interface
        // allowing a loading dependency to be passed in to it, then another view model (typically the root or other
        // parent) could pass the child a loading dependency from its own viewModelInit handler. This feature is used
        // in the unit testing of this module.
        const common = this._common;
        this.traverse(vm => {
            vm._localState.enterPrepare();
            common.globalState.registerLoading(
                vm._localState.getSuccessEvent()
            );
        });
    }

    private preInitGuard(): void {
        if (this.isInitalised) {
            throw new Error(
                ErrorMessage.invalidOpAfterMethod(
                    INIT_METHOD_NAME,
                    VM_TYPE_NAME
                )
            );
        }
    }

    /**
     * A leaf-down traversal of the view model tree.
     */
    private traverse(fn: (vm: ViewModel) => void): void {
        if (this._children != undefined) {
            this._children.forEach((child: ViewModel) => {
                child.traverse(fn);
            });
        }
        fn(this);
    }

    /**
     * Some local loading dependencies (those registered via registerLoadingAfter) may not initiate until after
     * the view model's initialisaton sequence is over. Nonetheless, when they initiate they need to register
     * with the _localState, which requires it to be in the preparing state. This means that _localState can
     * only exit the preparing state when both the init sequence has completed AND when it has no further
     * queued dependencies.
     */
    private tryExitLocalPrepare(): void {
        if (this._localState.isDisposed) return;
        if (
            this.isInitalised &&
            this._localState.isPreparing &&
            this._pendingDeps === 0
        ) {
            this._localState.exitPrepare();
        }
    }
}
