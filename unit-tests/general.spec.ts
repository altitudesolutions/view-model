import { LoadingState } from "@altitude/busy-state";
import { Subject } from "rxjs";

import * as ExceptionMessage from "./exception-messages";
import * as Functions from "./functions";
import {
    ExposedViewModel,
    ImmediateCompositeViewModel,
    NopViewModel,
    RootViewModel
} from "./helpers";

interface ITempData {
    a: number;
    b: string;
    c: boolean;
}

describe("View model structure", () => {
    it("Should assign data values on composites", async () => {
        // This test simply confirms that all view models in a composition share the same data instance.
        const root = new RootViewModel<ITempData>();
        const a = new ImmediateCompositeViewModel(root, { key: "a", value: 1 });
        const b = new ImmediateCompositeViewModel(root, {
            key: "b",
            value: "test"
        });
        const c = new ImmediateCompositeViewModel(root, {
            key: "c",
            value: true
        });
        root.publicAggregate(a);
        a.publicAggregate(b);
        b.publicAggregate(c);
        root.construct();
        expect(root.publicData).toEqual(<any>{});
        root.init();
        await Functions.awaitLoading(root, LoadingState.Loaded);
        const expected: ITempData = {
            a: 1,
            b: "test",
            c: true
        };
        expect(root.publicData).toEqual(expected);
    });

    it("Should process busy", () => {
        const vm = new NopViewModel();
        const trigger1 = new Subject<void>();
        vm.init();
        expect(vm.isBusy).toBe(false);
        vm.publicRegisterBusy(trigger1);
        expect(vm.isBusy).toBe(true);
        trigger1.next();
        expect(vm.isBusy).toBe(false);
        const trigger2 = new Subject<void>();
        vm.publicRegisterBusy(trigger2);
        trigger1.next();
        expect(vm.isBusy).toBe(true);
        // No false positive
        trigger1.next();
        expect(vm.isBusy).toBe(true);
        trigger2.next();
        expect(vm.isBusy).toBe(false);
    });

    it("Should not allow aggregation to multiple parents", () => {
        const p1 = new NopViewModel();
        const p2 = new NopViewModel();
        const child = new NopViewModel();
        p1.publicAggregate(child);
        expect(child.publicParent)
            .withContext("Invalid parent")
            .toBe(p1);
        expect(() => {
            p2.publicAggregate(child);
        }).toThrowError(ExceptionMessage.ALREADY_AGGREGATED);
    });
    it("Should not allow aggregation of itself", () => {
        const p = new NopViewModel();
        expect(() => {
            p.publicAggregate(p);
        }).toThrowError(ExceptionMessage.CANNOT_AGGREGATE_SELF);
    });
    it("Should not allow aggregation after construct", () => {
        const p = new NopViewModel();
        const child = new NopViewModel();
        p.construct();
        expect(() => {
            p.publicAggregate(child);
        }).toThrowError(ExceptionMessage.INVALID_POST_CONSTRUCT);
    });
    it("Should not allow aggregation after init", () => {
        const p = new NopViewModel();
        const child = new NopViewModel();
        p.init();
        expect(() => {
            p.publicAggregate(child);
        }).toThrowError(ExceptionMessage.INVALID_POST_CONSTRUCT);
    });
    it("Should not allow init to be invoked twice", () => {
        const p = new NopViewModel();
        p.init();
        expect(() => {
            p.init();
        }).toThrowError(ExceptionMessage.INVALID_POST_INIT);
    });
    it("Should not allow construct to be invoked twice", () => {
        const p = new NopViewModel();
        p.construct();
        expect(() => {
            p.construct();
        }).toThrowError(ExceptionMessage.INVALID_POST_CONSTRUCT);
    });
    it("Should not allow init to be invoked on non-root view models", () => {
        const p = new NopViewModel();
        const child = new NopViewModel();
        p.publicAggregate(child);
        expect(() => {
            child.init();
        }).toThrowError(ExceptionMessage.ROOT_ONLY);
    });
    it("Should not allow construct to be invoked on non-root view models", () => {
        const p = new NopViewModel();
        const child = new NopViewModel();
        p.publicAggregate(child);
        expect(() => {
            child.construct();
        }).toThrowError(ExceptionMessage.ROOT_ONLY);
    });
    it("Should throw if post-construct methods and properties are invoked pre-construct", () => {
        const vm = new NopViewModel();
        let v: any;
        expect(() => {
            v = vm.busyState;
        }).toThrowError(ExceptionMessage.INVALID_PRE_CONSTRUCT);
        expect(() => {
            v = vm.publicData;
        }).toThrowError(ExceptionMessage.INVALID_PRE_CONSTRUCT);
        expect(() => {
            v = vm.disposer;
        }).toThrowError(ExceptionMessage.INVALID_PRE_CONSTRUCT);
        expect(() => {
            v = vm.loaded$;
        }).toThrowError(ExceptionMessage.INVALID_PRE_CONSTRUCT);
        expect(() => {
            vm.publicRegisterBusy(new Subject());
        }).toThrowError(ExceptionMessage.INVALID_PRE_CONSTRUCT);
    });
    it("Should NOT throw if post-construct methods and properties are invoked post-construct", () => {
        const vm = new NopViewModel();
        vm.construct();
        let v: any;
        function test() {
            v = vm.busyState;
            v = vm.publicData;
            v = vm.disposer;
            v = vm.loaded$;
            vm.publicRegisterBusy(new Subject());

            return true;
        }
        expect(test()).toBe(true);
    });
    it("Should throw registerLoading methods invoked pre-init", () => {
        expectInitialising_registerLoadingSet(new NopViewModel());
    });
    it("Should throw registerLoading methods invoked post-init", () => {
        const vm = new NopViewModel();
        vm.init();
        expectInitialising_registerLoadingSet(vm);
    });
    it("Safe post-construct properties should return sane values pre-construct", () => {
        const vm = new NopViewModel();
        expect(vm.isBusy).toBe(false);
        expect(vm.isLoaded).toBe(false);
        expect(vm.isStatic).toBe(false);
    });
});

function expectInitialising_registerLoadingSet(vm: ExposedViewModel): void {
    const s = new Subject();
    expect(() => {
        vm.publicRegisterLoading(s);
    }).toThrowError(ExceptionMessage.INVALID_NOT_INITING);
    expect(() => {
        vm.publicRegisterLoadingAfter([s], () => s);
    }).toThrowError(ExceptionMessage.INVALID_NOT_INITING);
    expect(() => {
        vm.publicRegisterLoadingSequence(() => s);
    }).toThrowError(ExceptionMessage.INVALID_NOT_INITING);
    expect(() => {
        vm.publicRegisterLoadingSequenceAfter([s], () => s);
    }).toThrowError(ExceptionMessage.INVALID_NOT_INITING);
}
