import * as ErrorMessage from "@altitude/error-message";

export const INVALID_PRE_INIT = ErrorMessage.invalidOperation(
    `The operation is invalid before the "ViewModel.init" method has been called.`
);
export const INVALID_POST_INIT = ErrorMessage.invalidOperation(
    `The operation is invalid after the "ViewModel.init" method has been called.`
);
export const INVALID_PRE_CONSTRUCT = ErrorMessage.invalidOperation(
    `The operation is invalid before the "ViewModel.construct" method has been called.`
);
export const INVALID_POST_CONSTRUCT = ErrorMessage.invalidOperation(
    `The operation is invalid after the "ViewModel.construct" method has been called.`
);
export const ROOT_ONLY = ErrorMessage.invalidOperation(
    "The operation may only be performed on the root ViewModel."
);
export const CANNOT_AGGREGATE_SELF = ErrorMessage.argument(
    "viewModel",
    "Cannot aggregate itself."
);
export const ALREADY_AGGREGATED = ErrorMessage.argument(
    "viewModel",
    "The instance has already been aggregated."
);
export const INVALID_NOT_INITING = ErrorMessage.invalidOpOutsideState(
    "initialising",
    "ViewModel"
);
