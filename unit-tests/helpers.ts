import * as Async from "@altitude/async";
import { argumentNull } from "@altitude/error-message";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";

import { ViewModel, ViewModelDependency } from "../src";

// tslint:disable: max-classes-per-file

export class Sequencer {
    readonly output: string[] = [];
    delay(n: string, d: number): Observable<void> {
        return Async.awaitDelay<void>(d).pipe(tap(() => this.output.push(n)));
    }

    delaySeq(n: string, d: number): () => Observable<void> {
        return () =>
            Async.awaitDelay<void>(d).pipe(tap(() => this.output.push(n)));
    }
    expect(output: string[]): void {
        expect(this.output).toEqual(output);
    }
    imm(n: string): Observable<void> {
        return Async.immediate<void>().pipe(tap(() => this.output.push(n)));
    }
}

export interface ITestDataValue {
    key: string;
    value: any;
}

export abstract class ExposedViewModel<TData = any> extends ViewModel<TData> {
    label: string | undefined;
    get publicData(): TData {
        return this.data;
    }

    get publicParent(): ViewModel {
        // tslint:disable-next-line: no-unsafe-any
        return (<any>this)._parent;
    }
    get publicRoot(): ViewModel {
        // tslint:disable-next-line: no-unsafe-any
        return (<any>this)._root;
    }

    publicAggregate(child: ViewModel): void {
        this.aggregate(child);
    }

    publicRegisterBusy(action: Observable<any>): void {
        this.registerBusy(action);
    }

    publicRegisterLoading(...actions: Observable<any>[]): void {
        this.registerLoading(...actions);
    }

    publicRegisterLoadingAfter(
        dependencies: ViewModelDependency[],
        action: Async.AwaitAction
    ): void {
        this.registerLoadingAfter(dependencies, action);
    }

    publicRegisterLoadingSequence(...actions: Async.AwaitAction[]): void {
        this.registerLoadingSequence(...actions);
    }

    publicRegisterLoadingSequenceAfter(
        dependencies: ViewModelDependency[],
        ...actions: Async.AwaitAction[]
    ): void {
        this.registerLoadingSequenceAfter(dependencies, ...actions);
    }
}

export class NopViewModel extends ExposedViewModel {
    constructor() {
        super();
    }
    // tslint:disable-next-line: prefer-function-over-method no-empty
    viewModelInit(): void {}
}

export class TestViewModelTracker {
    private readonly _initSequence = 0;
    private readonly _loadedSequence = 0;
    private readonly _localLoadedSequence = 0;

    get initSequence(): number {
        return this._initSequence;
    }

    get loadedSequence(): number {
        return this._loadedSequence;
    }

    get localLoadedSequence(): number {
        return this._localLoadedSequence;
    }
}

interface ITracker {
    _initSequence: number;
    _loadedSequence: number;
    _localLoadedSequence: number;
}

export abstract class TestViewModel<TData = any> extends ExposedViewModel<
    TData
> {
    get initSequence(): number {
        return this._initSequence;
    }

    get loadedSequence(): number {
        return this._loadedSequence;
    }

    get localLoadedSequence(): number {
        return this._localLoadedSequence;
    }

    vmOnInit?: (viewModel: TestViewModel<TData>) => void;
    vmOnLoaded?: (viewModel: TestViewModel<TData>) => void;
    vmOnLocalLoaded?: (viewModel: TestViewModel<TData>) => void;
    private _initSequence = -1;
    private _loadedSequence = -1;
    private _localLoadedSequence = -1;
    private readonly _tracker: ITracker;

    constructor(tracker: TestViewModelTracker) {
        super();
        if (tracker == undefined) {
            throw new Error(argumentNull("tracker"));
        }
        this._tracker = <any>tracker;
    }

    setLabel(label: string): TestViewModel<TData> {
        this.label = label;

        return this;
    }

    protected onLoaded(): void {
        this._loadedSequence = this._tracker._loadedSequence += 1;
        if (this.vmOnLoaded != undefined) this.vmOnLoaded(this);
    }

    protected onLocalLoaded(): void {
        this._localLoadedSequence = this._tracker._localLoadedSequence += 1;
        if (this.vmOnLocalLoaded != undefined) this.vmOnLocalLoaded(this);
    }
    protected abstract testInit(): void;

    protected viewModelInit(): void {
        this._initSequence = this._tracker._initSequence += 1;
        if (this.vmOnInit != undefined) {
            this.vmOnInit(this);
        }

        this.testInit();
    }
}

export class RootViewModel<TData = any> extends TestViewModel<TData> {
    constructor() {
        super(new TestViewModelTracker());
        this.label = "root";
    }
    // tslint:disable-next-line: prefer-function-over-method no-empty
    testInit(): void {}
}

export abstract class CompositeBase<TData> extends TestViewModel<TData> {
    constructor(root: RootViewModel<TData>, readonly value?: ITestDataValue) {
        // tslint:disable-next-line: no-unsafe-any
        super((<any>root)._tracker);
    }

    protected applyValue(trigger: Observable<any>): Observable<any> {
        if (this.value != undefined) {
            this.disposer.subscribeOnce(trigger, () => {
                // strict compilation rules require a second undefined check in this scope
                if (this.value != undefined) {
                    (<any>this.data)[this.value.key] = this.value.value;
                }
            });
        }

        return trigger;
    }
}

export class NopCompositeViewModel<TModel> extends CompositeBase<TModel> {
    constructor(root: RootViewModel<TModel>) {
        super(root);
    }

    // tslint:disable-next-line: prefer-function-over-method no-empty
    protected testInit(): void {}
}

export class DelayCompositeViewModel<TData> extends CompositeBase<TData> {
    constructor(
        private readonly delay: number,
        root: RootViewModel<TData>,
        readonly value?: ITestDataValue
    ) {
        super(root, value);
    }

    protected testInit(): void {
        this.registerLoading(this.applyValue(Async.awaitDelay(this.delay)));
    }
}

export class ImmediateCompositeViewModel<TData> extends CompositeBase<TData> {
    constructor(root: RootViewModel<TData>, readonly value?: ITestDataValue) {
        super(root, value);
    }

    protected testInit(): void {
        this.registerLoading(this.applyValue(Async.immediate()));
    }
}

export class DepsCompositeViewModel<TData> extends CompositeBase<TData> {
    constructor(
        private readonly deps: ViewModelDependency[],
        root: RootViewModel<TData>,
        readonly value?: ITestDataValue
    ) {
        super(root, value);
    }

    protected testInit(): void {
        this.registerLoadingAfter(this.deps, () =>
            this.applyValue(Async.immediate())
        );
    }
}
