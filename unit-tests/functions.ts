import * as Async from "@altitude/async";
import { LoadingState } from "@altitude/busy-state";
import { map } from "rxjs/operators";

import { ViewModel } from "../src";

export function expectLoadingState(
    target: ViewModel,
    state: LoadingState
): void {
    const text = enumName(LoadingState, state);
    expect(enumName(LoadingState, target.busyState.loadingState))
        .withContext(`Expected loadingState to be "${text}".`)
        .toBe(text);
}

export async function awaitLoaded(target: ViewModel) {
    return target.loaded$.toPromise();
}

export async function awaitLoading(
    target: ViewModel,
    state: LoadingState,
    context?: string
) {
    const obs = Async.observeOnce(target.busyState.loadingState$).pipe(
        map(v => enumName(LoadingState, state))
    );

    await obs
        .toPromise()
        .then(async () =>
            state === LoadingState.Loaded
                ? target.loaded$.toPromise()
                : Promise.resolve()
        );

    const text = enumName(LoadingState, state);

    expect(target.isLoaded)
        .withContext(prepend(context, "Unepected isLoaded property value."))
        .toBe(state === LoadingState.Loaded);

    // tslint:disable-next-line: no-floating-promises
    expectAsync(obs.toPromise())
        .withContext(
            prepend(context, `Expected loadingState$ to emit ${text}.`)
        )
        .toBeResolvedTo(text);
}

function enumName(enumType: any, enumValue: any): string | undefined {
    if (enumValue == undefined) return undefined;
    for (const n in LoadingState) {
        // tslint:disable-next-line: no-unsafe-any
        if (enumType[n] === enumValue) return n;
    }

    return undefined;
}

function prepend(prefix: string | undefined, detail: string): string {
    // tslint:disable-next-line: prefer-template
    return prefix != undefined ? prefix + " " + detail : detail;
}
