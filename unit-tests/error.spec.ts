import { LoadingState } from "@altitude/busy-state";
import { Subject, throwError } from "rxjs";

import * as Functions from "./functions";
import { RootViewModel } from "./helpers";

// tslint:disable: no-magic-numbers

describe("View model error tests", async () => {
    it("Should emit error from busy state", async () => {
        let error: string | undefined;
        const vm = new RootViewModel();
        vm.construct();
        vm.error$.subscribe((e: string) => {
            error = e;
        });
        vm.publicRegisterBusy(throwError("Failed"));
        expect(error).toEqual("Failed");
    });

    it("Should emit registerLoading error", async () => {
        let error: string | undefined;
        const vm = new RootViewModel();
        vm.construct();
        vm.error$.subscribe((e: string) => {
            error = e;
        });
        vm.vmOnInit = () => {
            vm.publicRegisterLoading(throwError("Failed"));
        };
        vm.init();
        expect(error).toEqual("Failed");
        await Functions.awaitLoading(vm, LoadingState.Failed);
    });

    it("Should emit registerLoadingAfter error", async () => {
        const trigger = new Subject<void>();
        let error: string | undefined;
        const vm = new RootViewModel();
        vm.construct();
        vm.error$.subscribe((e: string) => {
            error = e;
        });
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingAfter([trigger], () =>
                throwError("Failed")
            );
        };
        vm.init();
        trigger.next();
        await Functions.awaitLoading(vm, LoadingState.Failed);
        expect(error).toEqual("Failed");
    });
});
