import * as Async from "@altitude/async";
import { LoadingState } from "@altitude/busy-state";
import { Subject, throwError } from "rxjs";

import * as Functions from "./functions";
import { RootViewModel } from "./helpers";

// tslint:disable: no-magic-numbers

describe("View model dispose tests", async () => {
    it("Should not process busy after disposed", async () => {
        let isBusy: boolean | undefined;
        const vm = new RootViewModel();
        const trigger = new Subject<void>();
        vm.construct();
        vm.busyState.busyState$.subscribe(v => (isBusy = v));
        vm.publicRegisterBusy(trigger);
        expect(isBusy).toEqual(true);
        vm.dispose();
        trigger.next();
        expect(isBusy).toEqual(true);
    });

    it("Should not process loading when disposed called during init", async () => {
        let ls: LoadingState | undefined;
        const vm = new RootViewModel();
        const trigger = new Subject<void>();
        vm.construct();
        vm.busyState.loadingState$.subscribe(v => (ls = v));
        vm.vmOnInit = () => {
            vm.publicRegisterLoading(trigger);
            vm.dispose();
        };
        vm.init();
        trigger.next();
        expect(ls).toEqual(<any>LoadingState.Initial);
    });

    it("Should not process loading when disposed called after init", async () => {
        let ls = LoadingState.Initial;
        const vm = new RootViewModel();
        const trigger = new Subject<void>();
        vm.construct();
        vm.busyState.loadingState$.subscribe(v => (ls = v));
        vm.vmOnInit = () => {
            vm.publicRegisterLoading(trigger);
        };
        vm.init();
        // Surrender execution to allow LoadingState.Loading to emit
        await Async.awaitDelay(10).toPromise();
        vm.dispose();
        trigger.next();
        expect(ls).toEqual(<any>LoadingState.Loading);
    });

    it("Should not process loadingAfter when disposed called after init", async () => {
        let ls = LoadingState.Initial;
        const vm = new RootViewModel();
        const trigger = new Subject<void>();
        vm.construct();
        vm.busyState.loadingState$.subscribe(v => (ls = v));
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingAfter([trigger], () => Async.immediate());
        };
        vm.init();
        // Surrender execution to allow LoadingState.Loading to emit
        await Async.awaitDelay(50).toPromise();
        vm.dispose();
        trigger.next();
        expect(ls).toEqual(<any>LoadingState.Loading);
    });

    it("Should emit registerLoadingAfter error", async () => {
        const trigger = new Subject<void>();
        let error: string | undefined;
        const vm = new RootViewModel();
        vm.construct();
        vm.error$.subscribe((e: string) => {
            error = e;
        });
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingAfter([trigger], () =>
                throwError("Failed")
            );
        };
        vm.init();
        trigger.next();
        await Functions.awaitLoading(vm, LoadingState.Failed);
        expect(error).toEqual("Failed");
    });
});
