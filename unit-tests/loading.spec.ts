import * as Async from "@altitude/async";
import { LoadingState } from "@altitude/busy-state";
import { Subject } from "rxjs";

import * as Functions from "./functions";
import { NopCompositeViewModel, RootViewModel, Sequencer } from "./helpers";
import {
    LoadedSequenceTester,
    LocalLoadedSequenceTester
} from "./sequence-tester";

// tslint:disable: no-magic-numbers

type ProcessOrder = "standard-order" | "inverse-order";
type AsyncMode = "synchronous" | "asynchronous";

describe("View model loading", () => {
    it("Should registerLoadingAfter with empty dependency set", async () => {
        const vm = new RootViewModel();
        const s = new Sequencer();
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingAfter([], () => [
                s.imm("r1"),
                s.imm("r2"),
                s.imm("r3")
            ]);
        };
        vm.init();
        await Functions.awaitLoaded(vm);
        s.expect(["r1", "r2", "r3"]);
    });
    it("registerLoadingAfter should load in order of ViewModel dependencies (synchronous standard order)", async () => {
        await vmDepsOrderTest("standard-order", "synchronous");
    });
    it("registerLoadingAfter should load in order of ViewModel dependencies (synchronous inverse order)", async () => {
        await vmDepsOrderTest("inverse-order", "synchronous");
    });
    it("registerLoadingAfter should load in order of ViewModel dependencies (asynchronous standard order)", async () => {
        await vmDepsOrderTest("standard-order", "asynchronous");
    });
    it("registerLoadingAfter should load in order of ViewModel dependencies (asynchronous inverse order)", async () => {
        await vmDepsOrderTest("inverse-order", "asynchronous");
    });
    it("registerLoadingAfter should load in order of Observable dependencies (synchronous standard order)", async () => {
        await obsDepsOrderTest("standard-order", "synchronous");
    });
    it("registerLoadingAfter should load in order of Observable dependencies (synchronous inverse order)", async () => {
        await obsDepsOrderTest("inverse-order", "synchronous");
    });
    it("registerLoadingAfter should load in order of Observable dependencies (asynchronous standard order)", async () => {
        await obsDepsOrderTest("standard-order", "asynchronous");
    });
    it("registerLoadingAfter should load in order of Observable dependencies (asynchronous inverse order)", async () => {
        await obsDepsOrderTest("inverse-order", "asynchronous");
    });
    it("Should complete loading on single view model with no registered loading events", async () => {
        const root = new RootViewModel();
        expect(root.isLoaded)
            .withContext("View model reported loaded before init.")
            .toBe(false);
        root.init();
        await Functions.awaitLoading(root, LoadingState.Loaded);
    });
    it("Should complete loading with single composite with no registered loading events", async () => {
        const root = new RootViewModel();
        const comp = new NopCompositeViewModel(root);
        root.publicAggregate(comp);
        expect(root.isLoaded)
            .withContext("Root view model reported loaded before init.")
            .toBe(false);
        expect(comp.isLoaded)
            .withContext("Aggregated view model reported loaded before init.")
            .toBe(false);
        root.init();
        await Functions.awaitLoading(
            root,
            LoadingState.Loaded,
            "Root view model did not load."
        );
        await Functions.awaitLoading(
            comp,
            LoadingState.Loaded,
            "Aggregated view model did not load."
        );
        await expectAsync(root.loaded$.toPromise()).toBeResolved();
    });
    it("Should invoke viewModel onLoaded on outermost aggregated nodes first when resolved synchronously", async () => {
        const tester = new LoadedSequenceTester(3, 3, false);
        await tester.test();
        expect(tester.count)
            .withContext("Unexpected node count")
            .toBe(40);
    });
    it("Should invoke viewModel onLoaded on outermost aggregated nodes first when resolved asynchronously", async () => {
        const tester = new LoadedSequenceTester(3, 3, true);
        await tester.test();
        expect(tester.count)
            .withContext("Unexpected node count")
            .toBe(40);
    }, 500);
    it("Should invoke viewModel onLocalLoaded on outermost aggregated nodes first when resolved synchronously", async () => {
        const tester = new LocalLoadedSequenceTester(3, 3, false);
        await tester.test();
        expect(tester.count)
            .withContext("Unexpected node count")
            .toBe(40);
    });
    it("Should invoke viewModel onLocalLoaded in root-up order when dependencies resolve in that order", async () => {
        const tester = new LocalLoadedSequenceTester(3, 3, true);
        await tester.test();
        expect(tester.count)
            .withContext("Unexpected node count")
            .toBe(40);
    }, 1000);
});

async function vmDepsOrderTest(order: ProcessOrder, mode: AsyncMode) {
    const obsFn =
        mode === "asynchronous"
            ? () => Async.awaitDelay(50)
            : () => Async.immediate();
    const sequence = order === "standard-order" ? [1, 2, 3, 4] : [4, 3, 2, 1];
    const root = new RootViewModel();
    const a = new NopCompositeViewModel(root).setLabel("a");
    const b = new NopCompositeViewModel(root).setLabel("b");
    const c = new NopCompositeViewModel(root).setLabel("c");
    root.publicAggregate(a);
    root.publicAggregate(b);
    root.publicAggregate(c);
    root.vmOnInit = () => {
        if (order === "standard-order") {
            c.publicRegisterLoadingAfter([b], obsFn);
            b.publicRegisterLoadingAfter([a], obsFn);
            a.publicRegisterLoadingAfter([root], obsFn);
            root.publicRegisterLoading(obsFn());
        } else {
            root.publicRegisterLoadingAfter([a], obsFn);
            a.publicRegisterLoadingAfter([b], obsFn);
            b.publicRegisterLoadingAfter([c], obsFn);
            c.publicRegisterLoading(obsFn());
        }
    };
    root.init();
    await Functions.awaitLoaded(root);
    expect(root.localLoadedSequence).toBe(sequence[0]);
    expect(a.localLoadedSequence).toBe(sequence[1]);
    expect(b.localLoadedSequence).toBe(sequence[2]);
    expect(c.localLoadedSequence).toBe(sequence[3]);
}

async function obsDepsOrderTest(order: ProcessOrder, mode: AsyncMode) {
    const obsFn =
        mode === "asynchronous"
            ? () => Async.awaitDelay(50)
            : () => Async.immediate();
    const sequence = order === "standard-order" ? [1, 2, 3, 4] : [4, 3, 2, 1];
    const root = new RootViewModel();
    const a = new NopCompositeViewModel(root).setLabel("a");
    const b = new NopCompositeViewModel(root).setLabel("b");
    const c = new NopCompositeViewModel(root).setLabel("c");
    const t1 = new Subject();
    const t2 = new Subject();
    const t3 = new Subject();
    const t4 = new Subject();

    root.publicAggregate(a);
    root.publicAggregate(b);
    root.publicAggregate(c);
    root.vmOnInit = () => {
        if (order === "standard-order") {
            root.publicRegisterLoadingAfter([t1], obsFn);
            a.publicRegisterLoadingAfter([t2], obsFn);
            b.publicRegisterLoadingAfter([t3], obsFn);
            c.publicRegisterLoadingAfter([t4], obsFn);
        } else {
            root.publicRegisterLoadingAfter([t4], obsFn);
            a.publicRegisterLoadingAfter([t3], obsFn);
            b.publicRegisterLoadingAfter([t2], obsFn);
            c.publicRegisterLoadingAfter([t1], obsFn);
        }
    };
    root.init();
    await nextAndComplete(t1)
        .then(() => nextAndComplete(t2))
        .then(() => nextAndComplete(t3))
        .then(() => nextAndComplete(t4))
        .then(() => Functions.awaitLoaded(root));
    expect(root.localLoadedSequence).toBe(sequence[0]);
    expect(a.localLoadedSequence).toBe(sequence[1]);
    expect(b.localLoadedSequence).toBe(sequence[2]);
    expect(c.localLoadedSequence).toBe(sequence[3]);
}

async function nextAndComplete(s: Subject<any>): Promise<any> {
    s.next();
    s.complete();

    return s.toPromise();
}
