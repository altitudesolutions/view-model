import { Subject } from "rxjs";
import { filter, take } from "rxjs/operators";

import {
    DelayCompositeViewModel,
    ImmediateCompositeViewModel,
    RootViewModel,
    TestViewModel
} from "./helpers";

// tslint:disable: max-classes-per-file no-magic-numbers

export interface ISequenceTesterPartialNode {
    actualSequence: number;
    depth: number;
    expectedSequence: number;
    index: number;
}

export interface ISequenceTesterNode extends ISequenceTesterPartialNode {
    children: ISequenceTesterNode[];
    vm: TestViewModel;
}

export abstract class ViewModelSequenceTester {
    get count(): number {
        return this._count;
    }

    get root(): ISequenceTesterNode {
        return this._root;
    }
    private _count = 0;
    private _root: ISequenceTesterNode;
    private _rootVm: RootViewModel<void>;
    private _sequence = 0;

    protected constructor(
        private readonly depth: number,
        private readonly leafs: number
    ) {}

    async test() {
        this._root = this.createNode(0, 0);
        this._rootVm = new RootViewModel();
        this._root.vm = this._rootVm;
        this.buildRecursive(this._root, 1);
        this.decorateRecursive(this._root);
        this.applyStandardSequence();

        this._sequence = 0;
        this._rootVm.init();

        this.init();
        await expectAsync(this._rootVm.loaded$.toPromise()).toBeResolved();
        this.validateRecursive(this._root);
        expect(this._root.vm.loadedSequence)
            .withContext(
                "Expected root loaded sequence to equal total leaf count"
            )
            .toBe(this.count);
        expect(this._root.vm.isLoaded)
            .withContext("Expected root view model to be loaded")
            .toBe(true);
    }

    protected abstract createViewModel(
        root: RootViewModel<void>,
        node: ISequenceTesterPartialNode
    ): TestViewModel<void>;
    protected abstract decorateNode(node: ISequenceTesterNode): void;
    // tslint:disable-next-line: no-empty
    protected init(): void {}

    protected nextSequence(): number {
        this._sequence += 1;

        return this._sequence;
    }

    private applyStandardSequence(): void {
        let s = 1;
        const f = (n: ISequenceTesterNode) => {
            n.children.forEach(c => {
                f(c);
            });
            n.expectedSequence = s;
            s += 1;
        };
        f(this._root);
    }

    private buildRecursive(parent: ISequenceTesterNode, depth: number): void {
        if (depth > this.depth) return;
        for (let i = 0; i < this.leafs; i += 1) {
            const child = this.createNode(depth, i);
            const vm = this.createViewModel(this._rootVm, child);
            child.vm = vm;
            parent.vm.publicAggregate(vm);
            parent.children.push(child);
            this.buildRecursive(child, depth + 1);
        }
    }

    private createNode(depth: number, index: number): ISequenceTesterNode {
        this._count += 1;
        const r = {
            actualSequence: 0,
            children: [],
            depth,
            expectedSequence: 0,
            index,
            vm: undefined
        };

        return <any>(<unknown>r);
    }

    private decorateRecursive(node: ISequenceTesterNode): void {
        node.children.forEach(child => {
            this.decorateRecursive(child);
        });
        this.decorateNode(node);
    }

    // tslint:disable-next-line: prefer-function-over-method
    private validateRecursive(node: ISequenceTesterNode): void {
        node.children.forEach(c => this.validateRecursive(c));
        expect(node.expectedSequence)
            .withContext(
                // tslint:disable-next-line: max-line-length
                `Expected sequence ${node.expectedSequence} but found ${node.actualSequence} (node.depth=${node.depth}, node.index=${node.index})`
            )
            .toEqual(node.actualSequence);
    }
}

export class LoadedSequenceTester extends ViewModelSequenceTester {
    constructor(
        depth: number,
        leafs: number,
        private readonly doAsync: boolean
    ) {
        super(depth, leafs);
    }

    protected createViewModel(root: RootViewModel<void>): TestViewModel<void> {
        return this.doAsync
            ? new DelayCompositeViewModel(30, root)
            : new ImmediateCompositeViewModel(root);
    }

    protected decorateNode(node: ISequenceTesterNode): void {
        node.vm.vmOnLoaded = () => {
            node.actualSequence = this.nextSequence();
        };
    }
}

export class LocalLoadedSequenceTester extends ViewModelSequenceTester {
    private readonly _trigger: Subject<number>;

    constructor(depth: number, leafs: number, doAsync: boolean) {
        super(depth, leafs);
        if (doAsync) {
            this._trigger = new Subject<number>();
        }
    }

    protected createViewModel(root: RootViewModel<void>): TestViewModel<void> {
        return new ImmediateCompositeViewModel(root);
    }

    protected decorateNode(node: ISequenceTesterNode): void {
        node.vm.vmOnLocalLoaded = () => {
            node.actualSequence = this.nextSequence();
        };
    }

    protected init(): void {
        this.root.vm.vmOnInit = () => {
            // tslint:disable-next-line: no-console
            console.log("*** Hooked Root ViewModelOnInit");
            this.applyInverseSequence();
        };
        if (this._trigger != undefined) {
            const fn = (step: number) => {
                this._trigger.next(step);
                setTimeout(() => {
                    fn(step + 1);
                });
            };
            fn(1);
        }
    }

    private applyInverseSequence(): void {
        let s = 1;
        const f = (node: ISequenceTesterNode) => {
            node.expectedSequence = s;
            const obs = this._trigger.pipe(
                filter(n => n === node.expectedSequence),
                take(1)
            );
            node.vm.publicRegisterLoading(obs);
            s += 1;
            node.children.forEach(c => {
                f(c);
            });
        };
        f(this.root);
    }
}
