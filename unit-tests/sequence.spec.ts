import * as Functions from "./functions";
import { RootViewModel, Sequencer } from "./helpers";

// tslint:disable: no-magic-numbers

describe("View model other sequence tests", async () => {
    it("Should load registerLoadingSequenceAfter actions in sequence", async () => {
        const vm = new RootViewModel();
        const s = new Sequencer();
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingSequence(
                s.delaySeq("r1", 60),
                s.delaySeq("r2", 30),
                s.delaySeq("r3", 10)
            );
        };
        vm.init();
        await Functions.awaitLoaded(vm);
        s.expect(["r1", "r2", "r3"]);
    });
    it("Should registerLoadingSequenceAfter with empty dependency set", async () => {
        const vm = new RootViewModel();
        const s = new Sequencer();
        vm.vmOnInit = () => {
            vm.publicRegisterLoadingSequenceAfter(
                [],
                s.delaySeq("r1", 60),
                s.delaySeq("r2", 30),
                s.delaySeq("r3", 10)
            );
        };
        vm.init();
        await Functions.awaitLoaded(vm);
        s.expect(["r1", "r2", "r3"]);
    });
});
